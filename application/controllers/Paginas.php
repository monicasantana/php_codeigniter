<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//error_reporting(E_ERROR | E_PARSE);

class Paginas extends CI_Controller {
    
public function __construct()
{
    parent::__construct();
    // Your own constructor code
    $this->load->helper('url');
    $this->load->model('host_model');
}
    
public function get_allhost()
{
    $data = array ();
    $data['csvData'] = $this->host_model->database();
    $this->load->view('plantillas/header');
    $this->load->view('paginas/alojamientos', $data);
    $this->load->view('plantillas/footer');
}   

public function show_neigh() //MUESTRA
{
    $data = array (); //para array model
    $data ['csvData']= $this->host_model->database();
    
    if ($this->input->get ('neigh_search') == false){
        $data = array (); //para array model
        $data ['csvData']= $this->host_model->database();
        $final ['data'] = $data ['csvData'];
        $this->load->view('plantillas/header');
        $this->load->view('paginas/neigh', $data);
        $this->load->view('plantillas/footer');
    } //si llega vacío no hace nada
    else {//si llega un valor
        
        //array ALL
        $all = $this->host_model->database();
        
        //valor FORM
        $find = '';
        $find = $this->host_model->formN(); 
        
        //FOR con ALL y FORM
        $array=array(
        );
        for ($i = 1; $i <= count($all); $i++){
            if ($all[$i]['neighbourhood']==$find){
                array_push($array, $all[$i]);
            }
        }
        if (count($array)===0){
        $this->load->view('plantillas/header');
        $this->load->view('paginas/back');
        }
        else{$final ['data'] = $array;
        
        $this->load->view('plantillas/header');
        $this->load->view('paginas/neigh', $final);
        $this->load->view('plantillas/footer');
        
}
}
}
    
    public function show_all() //MUESTRA
{
    $data = array (); //para array model
    $data ['csvData']= $this->host_model->database();
    
    if ($this->input->get ('all_search') == false){
        $data = array (); //para array model
        $data ['csvData']= $this->host_model->database();
        $final ['data'] = $data ['csvData'];
        $this->load->view('plantillas/header');
        $this->load->view('paginas/all', $data);
        $this->load->view('plantillas/footer');
    } //si llega vacío no hace nada
    else {//si llega un valor
        
        //array ALL
        $all = $this->host_model->database();
        
        //valor FORM
        $find = '';
        $find = $this->host_model->formA(); 
        
        //FOR con ALL y FORM
        $array=array(
        );
        for ($i = 1; $i <= count($all); $i++){
            if ($all[$i]['id']==$find || $all[$i]['name']==$find || $all[$i]['host_name']==$find || $all[$i]['neighbourhood']==$find || $all[$i]['latitude']==$find || $all[$i]['longitude']==$find || $all[$i]['room_type']==$find || $all[$i]['price']==$find || $all[$i]['minimum_nights']==$find){
                array_push($array, $all[$i]);
            }
        }
        if (count($array)===0){
        $this->load->view('plantillas/header');
        $this->load->view('paginas/back');
        }
        else{$final ['data'] = $array;
        
        $this->load->view('plantillas/header');
        $this->load->view('paginas/all', $final);
        $this->load->view('plantillas/footer');
        
}
}
}
        public function show_price() //MUESTRA
{
    $data = array (); //para array model
    $data ['csvData']= $this->host_model->database();
            
    if ($this->input->get ('mi_search') == false || ('ma_search') == false){
        $data = array (); //para array model
        $data ['csvData']= $this->host_model->database();
        $final ['data'] = $data ['csvData'];
        $this->load->view('plantillas/header');
        $this->load->view('paginas/price', $data);
        $this->load->view('plantillas/footer');
    } //si llega vacío no hace nada
    else {//si llega un valor
        
        //array ALL
        $all = $this->host_model->database();
        
        //valor FORM
        $find1;
        $find1 = $this->host_model->miForm();
        $find2;
        $find2 = $this->host_model->maForm(); 
        
        //FOR con ALL y FORM
        $array=array(
        );
        for ($i = 1; $i <= count($all); $i++){
            if ($find1 <= $all[$i]['price'] && $all[$i]['price'] <= $find2){
                array_push($array, $all[$i]);
            }
        }
        if (count($array)===0){
        $this->load->view('plantillas/header');
        $this->load->view('paginas/back');
        }
        else{$final ['data'] = $array;
        
        $this->load->view('plantillas/header');
        $this->load->view('paginas/price', $final);
        $this->load->view('plantillas/footer');
        
}
}
}
    public function show_position() //MUESTRA
{
    $data = array (); //para array model
    $data ['csvData']= $this->host_model->database();
            
    if ($this->input->get ('la_search') == false || ('lo_search') == false){
        $data = array (); //para array model
        $data ['csvData']= $this->host_model->database();
        $final ['data'] = $data ['csvData'];
        $this->load->view('plantillas/header');
        $this->load->view('paginas/position', $data);
        $this->load->view('plantillas/footer');
    } //si llega vacío no hace nada
    else {//si llega un valor
        
        //array ALL
        $all = $this->host_model->database();
        
        //valor FORM
        $find1;
        $find1 = $this->host_model->laForm();
        $find2;
        $find2 = $this->host_model->loForm(); 
        
        //FOR con ALL y FORM
        $array=array(
        );
        for ($i = 1; $i <= count($all); $i++){
            if ($all[$i]['latitude'] === $find1 || $all[$i]['longitude'] === $find2){
                array_push($array, $all[$i]);
            }
        }
        if (count($array)===0){
        $this->load->view('plantillas/header');
        $this->load->view('paginas/back');
        }
        else{$final ['data'] = $array;
        
        $this->load->view('plantillas/header');
        $this->load->view('paginas/position', $final);
        $this->load->view('plantillas/footer');
        
}
}
}
}
