</div>

</main>

<footer class="footer">
    <div class="container">
        <span class="text-muted"><strong>&copy; 2018 - Todos los derechos reservados.</strong>
        Page rendered in <strong>{elapsed_time}</strong> seconds.
            <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?>
        </span>
    </div>
</footer>

<!-- librería JQUERY -->
<script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/jquery-3.3.1.min.js"></script>

<!-- librería DATATABLE -->
<script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/dist/js/tabla.js"></script>

</body>

</html>
