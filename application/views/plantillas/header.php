<!DOCTYPE html>
<html lang="es">

<head>
    <!-- Bootstrap required meta tags -->
    <meta charset="UTF-8">
    <meta name="description" content="Búsqueda de alojamiento en BCN">
    <meta name="author" content="UOC">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Airbnb en Barcelona</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url()?>assets/site/docs/4.1/examples/starter-template/starter-template.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/site/docs/4.1/examples/sticky-footer-navbar/sticky-footer-navbar.css" rel="stylesheet">
    
    <!-- TABLA https://datatables.net/examples/styling/display.html -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/dist/css/jquery.dataTables.min.css">
</head>

<body>
