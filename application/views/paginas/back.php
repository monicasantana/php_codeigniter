<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top"> <a class="navbar-brand" href="<?php echo site_url('paginas/get_allhost')?>">BCN | AIRBNB</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('paginas/show_all')?>">Host<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('paginas/show_neigh')?>">Neighbourhood</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('paginas/show_price')?>">Price</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('paginas/show_position')?>">Position</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.airbnb.es">Airbnb</a>
            </li>
        </ul>
    </div>
</nav>
<main role="main" class="container">

    <div>
<h3>No existen resultados para la búsqueda solicitada.</h3>
<button onclick="history.go(-1);">Back </button>