<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top"> <a class="navbar-brand" href="<?php echo site_url('paginas/get_allhost')?>">BCN | AIRBNB</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('paginas/show_all')?>">Host<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('paginas/show_neigh')?>">Neighbourhood</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('paginas/show_price')?>">Price</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url('paginas/show_position')?>">Position</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="https://www.airbnb.es">Airbnb</a>
            </li>
        </ul>
    </div>
</nav>

<main role="main" class="container">

    <div>
        <span class="starter-template">
            <h1>
                BCN | Airbnb
            </h1>
        </span>
        <table id="tableUser" class="display" style="width:100%">
            <thead>
                <td width="05%"><strong>ID</strong></td>
                <td width="10%"><strong>HOST</strong></td>
                <td width="15%"><strong>DESCRIPTION</strong></td>
                <td width="35%"><strong>NEIGHBOURDHOOD</strong></td>
                <td width="35%"><strong>LATITUDE</strong></td>
                <td width="35%"><strong>LONGITUDE</strong></td>
                <td width="20%"><strong>ROOM</strong></td>
                <td width="10%"><strong>PRICE</strong></td>
                <td width="5%"><strong>MINIMUN</strong></td>
            </thead>

            <?php foreach($csvData as $field){?>
            <tr>
                <td>
                    <?php echo $field['id']?>
                </td>
                <td>
                    <?php echo $field['host_name']?>
                </td>
                <td>
                    <?php echo $field['name']?>
                </td>
                <td>
                    <?php echo $field['neighbourhood']?>
                </td>
                <td>
                    <?php echo $field['latitude']?>
                </td>
                <td>
                    <?php echo $field['longitude']?>
                </td>
                <td>
                    <?php echo $field['room_type']?>
                </td>
                <td>
                    <?php echo $field['price']?>
                </td>
                <td>
                    <?php echo $field['minimum_nights']?>
                </td>

            </tr>
            <?php }?>
        </table>
