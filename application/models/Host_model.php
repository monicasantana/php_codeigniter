<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Host_model extends CI_Model {
    
    public function __construct()
            {
            // Call the Model constructor
            parent::__construct();
            }
    
    public function database(){
            $this->load->library('csvreader');
            $result = $this->csvreader->parse_file(base_url().'listings.csv');
            //$result = json_encode($convert);
            return $result;
            }

    public function formN(){ // GET       
            $searchN = $this->input->get ('neigh_search');
            return $searchN;
    }
    
    public function formA(){ // GET       
            $searchA = $this->input->get ('all_search');
            return $searchA;
    }
    
    public function miForm(){ // GET       
            $searchMi = $this->input->get ('mi_search');
            return $searchMi;
    }
    
    public function maForm(){ // GET       
            $searchMa = $this->input->get ('ma_search');
            return $searchMa;
    }
    
    public function laForm(){ // GET       
            $searchLa = $this->input->get ('la_search');
            return $searchLa;
    }
    
    public function loForm(){ // GET       
            $searchLo = $this->input->get ('lo_search');
            return $searchLo;
    }
}
?>